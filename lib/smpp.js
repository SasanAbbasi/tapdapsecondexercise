const format = require('util').format;

class Smpp {
    constructor(host, port){
        this.host = host;
        this.port = port;
    }

    _hide_password(password){
        let result = '';
        for(let i = 0; i < password.length; i++) result += '*';
        return result;
    }

    open(user, password){
        let hidden_password = this._hide_password(password);
        console.log(`Logged ${user} with password ${hidden_password} to ${this.host}:${this.port}`);
    }

    send(to, message){
        console.log(`Sent message to ${to}`);
        console.log(message);
    }

    close(){
        console.log('Connection close');
    }
}

module.exports = Smpp;

