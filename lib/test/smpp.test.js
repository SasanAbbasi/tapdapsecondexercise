const Smpp = require('../smpp');

let smpp = new Smpp('192.168.1.4', '4301');

test('should constructor works', () => {
    expect(smpp.host).toBe('192.168.1.4');
    expect(smpp.port).toBe('4301');
});

test('the characters of the password should be *s ', () => {
    let hidden_password = smpp._hide_password('randomPass');
    expect(hidden_password).toBe('**********');
});