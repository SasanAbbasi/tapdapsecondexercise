const Smpp = require('./smpp');
const SendMailConnection = require('./sendmail');

class SnippetToRefactor {

    send( type, message, to, subject){

        switch(type) {
            case'SMS': 
                let smpp = new Smpp('192.168.1.4', '4301');
                smpp.open('your-username', 'your-password');
                smpp.send(to, message);
                smpp.close();
                break;
            case 'mail':
                let smc = new SendMailConnection('mail.myserver.com');
                smc.prepare_message(to, message, subject);
                smc.send();
                smc.close();
                break;
            default:
                throw 'Invalid type';
                break;
        }
    }
}

module.exports = SnippetToRefactor;