const format = require('util').format;

class SendMailConnection{
    constructor(mail_server){
        this.mail_server = mail_server;
        console.log(`Created SendMailConnection for server ${mail_server}`);
    }

    prepare_message(to, message, subject){
        console.log(`Preparing message for ${to}`);
        console.log(`Subject: ${subject}`);
        console.log(message);
    }

    send(){
        console.log('Message sent!!!');
    }

    close(){
        console.log('Mail connection closed');
    }
}

module.exports = SendMailConnection;